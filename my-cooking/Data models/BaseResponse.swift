//
//  BaseResponse.swift
//  my-cooking
//
//  Created by Mayur Salvi on 31/01/22.
//  Copyright © 2022 ito. All rights reserved.
//

import Foundation
import UIKit


struct BaseResponse<Element:Decodable>: Decodable {

    var sectionTitle: String?
    var data: [Element]

}
