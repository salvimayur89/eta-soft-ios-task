//
//  RecipesListViewController.swift
//  my-cooking
//
//  Created by Vladas Drejeris on 16/09/2019.
//  Copyright © 2019 ito. All rights reserved.
//

import UIKit
import AlamofireImage

class RecipesListViewController: UIViewController {
    
    // MARK: - UI components
    
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Dependencies
    
    private let repository = RecipesRepository.shared
    
    private lazy var datasource: RecipeSectionDataSource<BaseResponse<Recipe>> = {
        let datasource = RecipeSectionDataSource<BaseResponse<Recipe>>()
        datasource.configureCell = { (element, cell,index) in
            
            let data = element.data[index.row]
            
            cell.textLabel?.text = data.title
            cell.textLabel?.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            cell.detailTextLabel?.text = data.dificulty.localizedString
            cell.detailTextLabel?.textColor = data.dificulty.color
            cell.imageView?.contentMode = .scaleAspectFill
            cell.imageView?.clipsToBounds = true
            if let imageUrl = data.image {
                cell.imageView?.af.setImage(withURL: imageUrl,
                                            placeholderImage: UIImage(named: "placeholder_small"),
                                            filter: AspectScaledToFitSizeFilter(size: CGSize(width: 32, height: 32)))
            } else {
                cell.imageView?.image = UIImage(named: "placeholder_small")
            }
        }
        
        datasource.didSelectElement = { [weak self] (element,index) in
            let data = element.data[index.row]
            self?.performSegue(withIdentifier: "ShowDetailsScreen", sender: data)
        }
        
        return datasource
    }()
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = datasource
        tableView.dataSource = datasource
        loadData()
        
    }
    
    private func loadData() {
        repository.getRecipeSectionData { [unowned self] (resultData) in
            switch resultData {
            case .success(let sections):
                self.datasource.elements = sections
                self.tableView.reloadData()
            case .failure(let error):
                self.handleError(error)
            }
        }
    }
    
    
    private func handleError(_ error: Error) {
        // There are no errors at the moment, therefore we don't need to implement this method.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let recipeViewController = segue.destination as? RecipeViewController,
           let recipe = sender as? Recipe {
            recipeViewController.recipe = recipe
        }
    }
    
    @IBAction func unwindFromRecipeViewController(_ segue: UIStoryboardSegue) {
        
    }
}
