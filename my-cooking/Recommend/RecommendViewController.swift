//
//  RecommendViewController.swift
//  my-cooking
//
//  Created by Mayur Salvi on 31/01/22.
//  Copyright © 2022 ito. All rights reserved.
//

import UIKit
import AlamofireImage

class RecommendViewController: UIViewController {
    
    // MARK: - UI components
    
    @IBOutlet public weak var tableView: UITableView!
    
    // MARK: - Dependencies
    
    private lazy var datasource: TableDatasource<Recipe> = {
        let datasource = TableDatasource<Recipe>()
        datasource.configureCell = { (element, cell,index) in
            
            cell.textLabel?.text = element.title
            cell.textLabel?.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            cell.detailTextLabel?.text = element.dificulty.localizedString
            cell.detailTextLabel?.textColor = element.dificulty.color
            cell.imageView?.contentMode = .scaleAspectFill
            cell.imageView?.clipsToBounds = true
            if let imageUrl = element.image {
                cell.imageView?.af.setImage(withURL: imageUrl,
                                            placeholderImage: UIImage(named: "placeholder_small"),
                                            filter: AspectScaledToFitSizeFilter(size: CGSize(width: 32, height: 32)))
            } else {
                cell.imageView?.image = UIImage(named: "placeholder_small")
            }
        }
        datasource.didSelectElement = { [weak self] (element,index) in
            self?.performSegue(withIdentifier: "ShowDetailsScreen", sender: element)
        }
        return datasource
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = datasource
        tableView.dataSource = datasource
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    private func loadData() {
        RecipesRepository.shared.getRecipeRecommendations() { [weak self] (result) in
            switch result {
            case .success(let recipes):
                self?.datasource.elements = recipes
                
                self?.tableView.reloadData()
            case .failure(let error):
                self?.handleError(error)
            }
        }
    }
    
    private func handleError(_ error: Error) {
        print(error)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let recipeViewController = segue.destination as? RecipeViewController,
           let recipe = sender as? Recipe {
            recipeViewController.recipe = recipe
        }
    }
    
    
}
