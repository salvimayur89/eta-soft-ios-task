//
//  RecipesRepository.swift
//  my-cooking
//
//  Created by Vladas Drejeris on 16/09/2019.
//  Copyright © 2019 ito. All rights reserved.
//

import Foundation
import UIKit

typealias LoadCallback<T> = (Result<T, Error>) -> Void

class RecipesRepository {
    
    static let shared: RecipesRepository = RecipesRepository()
    
    // MAKR: - State
    
    private var recipes: [Recipe] = []
    
    // MARK: - Making init private to avoid new repo object creation
    
    private init() {
        guard let url = Bundle.main.url(forResource: "Recipes", withExtension: "plist") else {
            return
        }
        guard let data = try? Data(contentsOf: url) else {
            return
        }
        
        do {
            recipes = try PropertyListDecoder().decode([Recipe].self, from: data)
        } catch {
            print(error)
            return
        }
    }
    
    // MARK: - Access
    
    /// Loads all available recipes.
    ///
    /// - Parameter completion: A callback that is called when loading is finished.
    func allRecipes(completion: LoadCallback<[Recipe]>) {
        completion(.success(recipes))
    }
    
    /// Loads an array of recipes with specified difficulty.
    ///
    /// - Parameters:
    ///   - difficulty: Specifies difficulty for the recipes to load.
    ///   - completion: A callback that is called when loading is finished.
    func recipes(withDifficulty difficulty: Difficulty, completion: LoadCallback<[Recipe]>) {
        let result = recipes.filter { $0.dificulty == difficulty }
        completion(.success(result))
    }
    
    /// Loads a recipe with specified id.
    ///
    /// - Parameters:
    ///   - id: Specifies the id of the recipe to load.
    ///   - completion: A callback that is called when loading is finished.
    func recipe(withId id: UUID, completion: LoadCallback<Recipe>) {
        guard let result = recipes.first(where: { $0.id == id }) else {
            completion(.failure(AppError.invalidId))
            return
        }
        completion(.success(result))
    }
}

extension RecipesRepository {
    
    /// Regrouping recipes based on its difficulty level.
    ///
    /// - Parameters:
    ///   - completion: A callback will return Regrouped Recipes w.r.t its difficulty.
    
    func getRecipeSectionData(completion: LoadCallback<[BaseResponse<Recipe>]>) {
        
        var list = [BaseResponse<Recipe>]()
        for diff in Difficulty.allCases {
            recipes(withDifficulty: diff) { (result) in
                switch result {
                case .success(let recipeData):
                    let section = BaseResponse(sectionTitle: diff.rawValue,
                                               data: recipeData)
                    list.append(section)
                    
                    // Compleltion wil be callled when data count is matched with total difficulty count
                    if Difficulty.allCases.count == list.count {
                        completion(.success(list))
                    }
                case .failure(let error):
                    completion(.failure(error))
                    return
                }
            }
        }
    }
    
    /// Loads a Recommended recipes based on users skill level
    ///
    /// - Parameters:
    ///   - completion: A callback that is called when loading is finished.
    func getRecipeRecommendations( completion: LoadCallback<[Recipe]>) {
        let dishRepo = DishesRepository.shared
        var dishDifficulty: Difficulty = .easy
        
        dishRepo.allDishes {(result) in
            switch result {
            case .success(let dishes):
                
                // If User has not tried any dishes yet, then by default Easy Recipes are recommended.
                if dishes.count == 0 {
                    dishDifficulty = .easy
                }  else {
                    dishDifficulty = getSkillFromResult(repo: dishRepo)
                }
                
                recipes(withDifficulty: dishDifficulty) { (result) in
                    switch result {
                    case .success(let recipeData):
                        completion(.success(recipeData))
                        return
                    case .failure(let error):
                        completion(.failure(error))
                        return
                    }
                }
            case .failure(let error):
                completion(.failure(error))
                return
            }
        }
    }
    
    /// Returns recommendation difficuty level based on User past several dishes skillset.
    ///
    /// - Parameters:
    ///   - repo: Passed shared object for Dish Repo.
    private func getSkillFromResult(repo:DishesRepository)-> Difficulty {
        
        var lowSkillDishes = [Dish]()
        var mediumSkillDishes = [Dish]()
        var highSkillDishes = [Dish]()
        
        
        /// Regrouping list of past tried based on cooking results.
        
        for cookResult in CookingResult.allCases {
            repo.dishes(withResult: cookResult) {(result) in
                switch result {
                case .success(let dishes):
                    switch cookResult {
                    case .totalDisaster:
                        lowSkillDishes += dishes
                    case .itWasEdible:
                        mediumSkillDishes += dishes
                    case .perfection:
                        highSkillDishes += dishes
                    }
                default :
                    break
                }
            }
        }
        
        /*
         *** Business Logic ***
         Skills will be judged on user's cooking result of past several cooking attempts.
         Total Disaster will be considered as low skill, It was edible will be considered as ormal skill & Perfection is
         highest possible skill.
         */
        
        /// Note: Business logic to judge Skill of user might differ based on actual requirement.
        
        if (mediumSkillDishes.count >= lowSkillDishes.count) && (mediumSkillDishes.count >= highSkillDishes.count) {
            return .normal
        } else if (highSkillDishes.count >= mediumSkillDishes.count) && (highSkillDishes.count >= lowSkillDishes.count) {
            return .hard
        } else {
            return .easy
        }
    }
}
