//
//  RecipeRepoTests.swift
//  my-cookingTests
//
//  Created by Mayur Salvi on 01/02/22.
//  Copyright © 2022 ito. All rights reserved.
//

import XCTest
@testable import my_cooking

class RecipeRepoTests: XCTestCase {
    
    var repo: RecipesRepository!
    
    override func setUp() {
        super.setUp()
        repo = RecipesRepository.shared
    }
    
    override func tearDown() {
        super.tearDown()
        repo = nil
    }
    
    func test_check_recipe_sections() {
        repo.getRecipeSectionData {(resultData) in
            switch resultData {
            case .success(let sections):
                XCTAssertTrue(sections.count > 0)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func test_check_data() {
        repo.getRecipeRecommendations() { (result) in
            switch result {
            case .success(let recipes):
                XCTAssertTrue(recipes.count > 0)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
            
        }
    }
    
}
