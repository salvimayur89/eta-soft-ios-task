//
//  RecommendTests.swift
//  my-cookingTests
//
//  Created by Mayur Salvi on 01/02/22.
//  Copyright © 2022 ito. All rights reserved.
//

import XCTest
@testable import my_cooking

class RecommendTests: XCTestCase {
    
    var recommendVCTest: RecommendViewController!
    var dataSource: TableDatasource<Recipe>!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.recommendVCTest = (storyboard.instantiateViewController(withIdentifier: "RecommendViewController") as! RecommendViewController)
        self.recommendVCTest.loadView()
        self.recommendVCTest.viewDidLoad()
        self.recommendVCTest.viewWillAppear(true)
        
        dataSource = TableDatasource<Recipe>()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        recommendVCTest = nil
        dataSource = nil
    }
    
    
    
    func testHasATableView() {
        XCTAssertNotNil(recommendVCTest.tableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(recommendVCTest.tableView.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(dataSource.conforms(to: UITableViewDelegate.self))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(recommendVCTest.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        
        
        XCTAssertTrue(dataSource.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(dataSource.responds(to: #selector(dataSource.numberOfSections(in:))))
        XCTAssertTrue(dataSource.responds(to: #selector(dataSource.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(dataSource.responds(to: #selector(dataSource.tableView(_:cellForRowAt:))))
    }
    
}
